#include <iostream>
#include <array>
#include <cassert>

constexpr auto board_size = 3;

using Player = char;
using Board_row = std::array<Player, board_size>;
using Board = std::array<Board_row, board_size>;



constexpr auto empty_board = Board{
		Board_row{' ', ' ', ' '},
		Board_row{' ', ' ', ' '},
		Board_row{' ', ' ', ' '}};


auto check_board(Board const & board) -> Player
{
	auto winner = ' ';
	
	for (int x = 0; x < board_size; x++) {
		auto p = board[0][x];
		if (p != ' ') {
			auto w = true;
			for (auto y = 1; y < board_size; y++) {
				if (p != board[y][x]) {
					w = false;
				}
			}
			if (w) {
				winner = p;
			}
		}
	}
	
	for (int y = 0; y < board_size; y++) {
		auto p = board[y][0];
		if (p != ' ') {
			auto w = true;
			for (auto x = 1; x < board_size; x++) {
				if (p != board[y][x]) {
					w = false;
				}
			}
			if (w) {
				winner = p;
			}
		}
	}
	
	{
		auto p = board[0][0];
		if (p != ' ') {
			auto w = true;
			for (auto xy = 1; xy < board_size; xy++) {
				if (p != board[xy][xy]) {
					w = false;
				}
			}
			if (w) {
				winner = p;
			}
		}
	}
	
	{
		auto p = board[0][board_size-1];
		if (p != ' ') {
			auto w = true;
			for (auto nxy = 1; nxy < board_size; nxy++) {
				if (p != board[nxy][board_size-1-nxy]) {
					w = false;
				}
			}
			if (w) {
				winner = p;
			}
		}
	}
	
	return winner;
}

void print_board(Board const & board)
{
	for (auto & line : board) {
		for (auto c : line) {
			std::cout << c;
		}
		std::cout << '\n';
	}
}

auto print_and_check_board(Board const & board) -> Player
{
	print_board(board);
	return check_board(board);
}

void place_mark(Board & board, int x, int y, Player player)
{
	board[y][x] = player;
}

void next_player(Player & player, Player player1 = 'x', Player player2 = 'o', Player error = 'e')
{
	player = (
			player == player1 ? player2 :
			player == player2 ? player1 :
			error);
}

auto main() -> int
{
	auto board = empty_board;
	auto player = Player{'x'};
	
	while (print_and_check_board(board) == ' ') {
		int x;
		int y;
		std::cout << "Player " << player << " enter coordinates (x y): ";
		std::cin >> x >> y;
		
		if (x >= board_size || y >= board_size) {
			std::cout << "ERROR: Field outside of board!\n";
		}
		else if (board[y][x] != ' ') {
			std::cout << "ERROR: Field already occupied!\n";
		}
		else {
			place_mark(board, x, y, player);
			next_player(player);
		}
	}
	
	std::cout << "Winner: " << check_board(board) << '\n';
}
